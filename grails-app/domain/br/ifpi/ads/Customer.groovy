package br.ifpi.ads

import grails.rest.Resource;

@Resource(uri='/customers', formats=['json'])
class Customer {
	
	String name;
	String email;
	String password;
	String address;
	String city;
	String country;
	
	static constraints = {
		
		name nullable: false, blank: false;
		email nullable: false, blank: false;
		password nullable: false, blank: false;
		
	}

}
