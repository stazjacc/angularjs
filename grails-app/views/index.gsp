<!DOCTYPE html>

<html ng-app="crudApp" xmlns="http://www.w3.org/1999/xhtml" lang="pt-br"
	xml:lang="pt-br">
	
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>CRUD AngularJS</title>
<meta name="viewport" content="width=device-width, initial-scale=1" />
<asset:link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />

<asset:stylesheet href="bootstrap/dist/css/bootstrap.min.css" />
<asset:stylesheet href="bootstrap/dist/css/bootstrap-theme.min.css" />

<asset:javascript src="jquery/dist/jquery.min.js" />
<asset:javascript src="bootstrap/dist/js/bootstrap.min.js" />
<asset:javascript src="application.js" />

<style type="text/css">
body>.container {
	padding: 60px 15px 0;
}
</style>
</head>


<body>
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<big> <i class="glyphicon glyphicon-log-out"></i>
				AngularJS </big>
			</div>
		</div>
	</nav>
	<div class="container">
		<div ng-view></div>
	</div>
</body>
</html>