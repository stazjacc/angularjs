var customerServices = angular.module( 'customerServices' , [ 'ngResource' ] );

customerServices.factory( 'Customer' , [ '$resource' , function( $resource ) {
	return $resource( '/AngularCRUD/customers/:id', { id: '@id' } , { update: { method: 'PUT' } } );
} ]);