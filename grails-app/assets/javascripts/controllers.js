var controllers = angular.module( 'controllers', [ 'customerServices' ] );

controllers.controller( 'ListController' , [ '$scope' , '$http', 'Customer' , function ( $scope , $http , Customer ) {
	$scope.customer = null;
	$scope.customers = Customer.query();
	
	$scope.onDeleteBtnClickedHandler = function( customer ){
		$scope.customer = customer;
		$( '#confirmation-modal' ).modal( 'show' );
	};
	
	$scope.deleteCustomer = function() {
		$scope.customer.$delete( function(){
			$( '#confirmation-modal' ).modal( 'hide' );
			$scope.customers = Customer.query();
			$( '#deleted-modal' ).modal( 'show' );
		});
		$scope.customer = null;
	};
}]);

controllers.controller( 'NewController' , [ '$scope' , '$http', 'Customer' , function ( $scope , $http , Customer ) {
	$scope.customer = {};

	$scope.onSubmitFormBtnClickedHandler = function() {
		Customer.save( $scope.customer , function(){
			$scope.onCustomerCreatedHandler();
		} );
	}
	
	$scope.onCustomerCreatedHandler = function() {
		$scope.customer = {};
		$( '#created-modal' ).modal( 'show' );
	}
}]);

controllers.controller( 'EditController' , [ '$scope' , '$http', '$routeParams', '$location', 'Customer' , function ( $scope , $http , $routeParams , $location , Customer ) {
	$scope.customer = Customer.get({ id: $routeParams.id});

	$scope.onSubmitFormBtnClickedHandler = function() {
		$scope.customer.$update( function(){
			$scope.onCustomerUpdatedHandler();
		} );
	}
	
	$scope.onCustomerUpdatedHandler = function() {
		$scope.customer = {};
		$( '#updated-modal' ).modal( 'show' );
	}
	
	$scope.a = function(){
		$('#updated-modal').on('hidden.bs.modal', function (e) {
			window.location = '/AngularCRUD'; 
		});
		$( '#updated-modal' ).modal( 'hide' );
	}
}]);