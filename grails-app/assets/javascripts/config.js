var crudApp = angular.module( 'crudApp', [ 'ngRoute', 'ngResource', 'customerServices', 'controllers' ] );

crudApp.config( [ '$routeProvider' , 
    function( $routeProvider ) {
        $routeProvider.
	        when( '/' , {
	            templateUrl: 'assets/partials/list.html'
	        }).
	        when( '/customer/new' , {
	            templateUrl: 'assets/partials/new.html'
	        }).
	        when( '/customer/edit/:id' , {
	            templateUrl: 'assets/partials/edit.html'
	        }).
	        otherwise({
	            redirectTo: function( routeParams , currentPath ) {
	                return '/';
	            }
	        });
	}
]);